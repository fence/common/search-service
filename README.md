# Search Service

Search Service created to translate query strings generated from a boolean search into SQL filters

For the full documentation please refer to these links: [Entity search guide](https://readthedocs.web.cern.ch/display/LHCBGLANCE/Entity+Search), [Search service guide](https://readthedocs.web.cern.ch/display/LHCBGLANCE/Entity+Search+backend+library+setup). 

## Getting started

Before we start, make sure you have run the migrations on your database. They can be found on the ```/example/migrations``` folder. Don't forget to change ```{{ HISTORY_SCHEMA_NAME }}``` to your history schema name and ```{{ SCHEMA_NAME }}``` to your main schema name. On the example we are also inserting a search type called ```surveys```. This goes in line with the example SQL queries we are using.

## Installing

To install the search service as one of your application's dependencies you can run the following command:

```bash
    composer require glance-project/search-service
```

## Setting up

You need to setup your SQL query and the .json configuration file for your search. The configuration file will be used to define the properties, cache location, lookup table and the location of your SQL query, which will be used to retrieve the data from the database. An example can be found on the ```/example/resources/survey-search.sql``` folder for the query and on the ```/example/resources/surveys.json``` folder for the configuration file.

When we define the properties on the configuration file, we include, alongside with the column name from the database, their type (e.g. "date") and the compatible operators (e.g. "contain", "not-contain", "=", "!=").

You also need to create the controller and setup the routes for the search service. An example can be found on the ```/example/Search/SearchController.php``` and ```/example/Search/SearchRoutes.php``` files. After they are set up, an user will be able to save, delete and find their saved searches (that's the reason for the database migrations).

With all this set up you are able to use the search service. For the client, please visit [this link](https://www.npmjs.com/package/@glance-project/search-client?activeTab=readme).

## Using the search service

On the ````/example/Survey/Web/SurveyController.php```` file you can find an example of how a controller communicates with the search service. An example GET request would look like this:

```
    'https://localhost:8080/rp-survey/api/surveys/search?offset=0&limit=50&queryString=(%2B%22description%22%0A%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%3D%0A%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%2B%22test%22%2B%2B)
```

(with the url decoded to ```
https://localhost:8080/rp-survey/api/surveys/search?offset=0&limit=50&queryString=(+"description"++++++++=+++++++"test"++)```

Would result in a response like this:

```
{
	"results": [
		{
			"id": "1389",
			"description": "test",
			"comments": null,
			"magnetOn": "N",
			"magnetOnAsString": "No",
			"startedOn": "04-AUG-22 01.52.00.000000 PM",
			"startedOnAsString": "2022-08-04 13:52",
			"beamOffAt": "04-AUG-22 01.52.00.000000 PM",
			"beamOffAtAsString": "2022-08-04 13:52",
			"ramsesLogLink": null,
			"responsiblePersonsNames": "Carlos Brito"
		}
	],
	"numberOfResults": "1"
}
```

# More info

This project was the subject of a presentation at the 26th International Conference On Computing In High Energy & Nuclear Physics (CHEP 2023). You can find the event [here](https://indico.jlab.org/event/459/contributions/11542/).

