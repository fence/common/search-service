CREATE TABLE SE_SEARCH_TYPE (
   ID integer generated by default on null as identity NOT NULL,
   NAME varchar2(50)  NOT NULL,
   AGENT_ID integer  NOT NULL,
   MODIFIED_ON date  NOT NULL,
   CONSTRAINT SE_SEARCH_TYPE_PK PRIMARY KEY (ID)
) ;

CREATE TABLE SE_SEARCH (
   ID integer generated by default on null as identity  NOT NULL,
   MEMBER_ID integer   NOT NULL,
   NAME varchar2(100)  NOT NULL,
   TYPE_ID integer  NOT NULL,
   FILTER clob  NULL,
   ITEMS_PER_PAGE integer  NOT NULL,
   SORT_BY varchar2(50)  NULL,
   SORT_DESC char(1)  NULL,
   RESULTS_HEADERS clob  NOT NULL,
   AGENT_ID integer  NOT NULL,
   MODIFIED_ON date  NOT NULL,
   CONSTRAINT SE_SEARCH_PK PRIMARY KEY (ID),
   CONSTRAINT FILTER_JSON CHECK (FILTER IS JSON),
   CONSTRAINT RESULTS_HEADERS_JSON CHECK (RESULTS_HEADERS IS JSON)
) ;

CREATE OR REPLACE TRIGGER HTR_SE_SEARCH_TYPE
AFTER INSERT OR UPDATE OR DELETE ON SE_SEARCH_TYPE
FOR EACH ROW
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
      INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH_TYPE (
          ID,
          NAME,
          MODIFIED_ON,
          AGENT_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.NAME,
          OPERATION_DATE,
          :NEW.AGENT_ID,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH_TYPE (
      ID,
      NAME,
      MODIFIED_ON,
      AGENT_ID,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.NAME,
      OPERATION_DATE,
      :OLD.AGENT_ID,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH_TYPE (
      ID,
      NAME,
      MODIFIED_ON,
      AGENT_ID,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.NAME,
      OPERATION_DATE,
      :NEW.AGENT_ID,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

CREATE OR REPLACE TRIGGER HTR_SE_SEARCH
AFTER INSERT OR UPDATE OR DELETE ON SE_SEARCH
FOR EACH ROW
DECLARE
  OPERATION_DATE TIMESTAMP(0);
BEGIN
  IF :NEW.MODIFIED_ON IS NULL THEN
    OPERATION_DATE := SYSTIMESTAMP;
  ELSE
    OPERATION_DATE := :NEW.MODIFIED_ON;
  END IF;
  IF UPDATING THEN
    IF COALESCE(TO_CHAR(:OLD.MEMBER_ID), 'null') <> COALESCE(TO_CHAR(:NEW.MEMBER_ID), 'null') THEN
      INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH (
          ID,
          MEMBER_ID,
          NAME,
          TYPE_ID,
          FILTER,
          ITEMS_PER_PAGE,
          SORT_BY,
          SORT_DESC,
          RESULTS_HEADERS,
          MODIFIED_ON,
          AGENT_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.NAME,
          :NEW.TYPE_ID,
          :NEW.FILTER,
          :NEW.ITEMS_PER_PAGE,
          :NEW.SORT_BY,
          :NEW.SORT_DESC,
          :NEW.RESULTS_HEADERS,
          OPERATION_DATE,
          :NEW.AGENT_ID,
          'U',
          'MEMBER_ID',
          TO_CHAR(:OLD.MEMBER_ID),
          TO_CHAR(:NEW.MEMBER_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.NAME), 'null') <> COALESCE(TO_CHAR(:NEW.NAME), 'null') THEN
      INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH (
          ID,
          MEMBER_ID,
          NAME,
          TYPE_ID,
          FILTER,
          ITEMS_PER_PAGE,
          SORT_BY,
          SORT_DESC,
          RESULTS_HEADERS,
          MODIFIED_ON,
          AGENT_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.NAME,
          :NEW.TYPE_ID,
          :NEW.FILTER,
          :NEW.ITEMS_PER_PAGE,
          :NEW.SORT_BY,
          :NEW.SORT_DESC,
          :NEW.RESULTS_HEADERS,
          OPERATION_DATE,
          :NEW.AGENT_ID,
          'U',
          'NAME',
          TO_CHAR(:OLD.NAME),
          TO_CHAR(:NEW.NAME)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.TYPE_ID), 'null') <> COALESCE(TO_CHAR(:NEW.TYPE_ID), 'null') THEN
      INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH (
          ID,
          MEMBER_ID,
          NAME,
          TYPE_ID,
          FILTER,
          ITEMS_PER_PAGE,
          SORT_BY,
          SORT_DESC,
          RESULTS_HEADERS,
          MODIFIED_ON,
          AGENT_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.NAME,
          :NEW.TYPE_ID,
          :NEW.FILTER,
          :NEW.ITEMS_PER_PAGE,
          :NEW.SORT_BY,
          :NEW.SORT_DESC,
          :NEW.RESULTS_HEADERS,
          OPERATION_DATE,
          :NEW.AGENT_ID,
          'U',
          'TYPE_ID',
          TO_CHAR(:OLD.TYPE_ID),
          TO_CHAR(:NEW.TYPE_ID)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.FILTER), 'null') <> COALESCE(TO_CHAR(:NEW.FILTER), 'null') THEN
      INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH (
          ID,
          MEMBER_ID,
          NAME,
          TYPE_ID,
          FILTER,
          ITEMS_PER_PAGE,
          SORT_BY,
          SORT_DESC,
          RESULTS_HEADERS,
          MODIFIED_ON,
          AGENT_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.NAME,
          :NEW.TYPE_ID,
          :NEW.FILTER,
          :NEW.ITEMS_PER_PAGE,
          :NEW.SORT_BY,
          :NEW.SORT_DESC,
          :NEW.RESULTS_HEADERS,
          OPERATION_DATE,
          :NEW.AGENT_ID,
          'U',
          'FILTER',
          TO_CHAR(:OLD.FILTER),
          TO_CHAR(:NEW.FILTER)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.ITEMS_PER_PAGE), 'null') <> COALESCE(TO_CHAR(:NEW.ITEMS_PER_PAGE), 'null') THEN
      INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH (
          ID,
          MEMBER_ID,
          NAME,
          TYPE_ID,
          FILTER,
          ITEMS_PER_PAGE,
          SORT_BY,
          SORT_DESC,
          RESULTS_HEADERS,
          MODIFIED_ON,
          AGENT_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.NAME,
          :NEW.TYPE_ID,
          :NEW.FILTER,
          :NEW.ITEMS_PER_PAGE,
          :NEW.SORT_BY,
          :NEW.SORT_DESC,
          :NEW.RESULTS_HEADERS,
          OPERATION_DATE,
          :NEW.AGENT_ID,
          'U',
          'ITEMS_PER_PAGE',
          TO_CHAR(:OLD.ITEMS_PER_PAGE),
          TO_CHAR(:NEW.ITEMS_PER_PAGE)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.SORT_BY), 'null') <> COALESCE(TO_CHAR(:NEW.SORT_BY), 'null') THEN
      INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH (
          ID,
          MEMBER_ID,
          NAME,
          TYPE_ID,
          FILTER,
          ITEMS_PER_PAGE,
          SORT_BY,
          SORT_DESC,
          RESULTS_HEADERS,
          MODIFIED_ON,
          AGENT_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.NAME,
          :NEW.TYPE_ID,
          :NEW.FILTER,
          :NEW.ITEMS_PER_PAGE,
          :NEW.SORT_BY,
          :NEW.SORT_DESC,
          :NEW.RESULTS_HEADERS,
          OPERATION_DATE,
          :NEW.AGENT_ID,
          'U',
          'SORT_BY',
          TO_CHAR(:OLD.SORT_BY),
          TO_CHAR(:NEW.SORT_BY)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.SORT_DESC), 'null') <> COALESCE(TO_CHAR(:NEW.SORT_DESC), 'null') THEN
      INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH (
          ID,
          MEMBER_ID,
          NAME,
          TYPE_ID,
          FILTER,
          ITEMS_PER_PAGE,
          SORT_BY,
          SORT_DESC,
          RESULTS_HEADERS,
          MODIFIED_ON,
          AGENT_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.NAME,
          :NEW.TYPE_ID,
          :NEW.FILTER,
          :NEW.ITEMS_PER_PAGE,
          :NEW.SORT_BY,
          :NEW.SORT_DESC,
          :NEW.RESULTS_HEADERS,
          OPERATION_DATE,
          :NEW.AGENT_ID,
          'U',
          'SORT_DESC',
          TO_CHAR(:OLD.SORT_DESC),
          TO_CHAR(:NEW.SORT_DESC)
      );
    END IF;

    IF COALESCE(TO_CHAR(:OLD.RESULTS_HEADERS), 'null') <> COALESCE(TO_CHAR(:NEW.RESULTS_HEADERS), 'null') THEN
      INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH (
          ID,
          MEMBER_ID,
          NAME,
          TYPE_ID,
          FILTER,
          ITEMS_PER_PAGE,
          SORT_BY,
          SORT_DESC,
          RESULTS_HEADERS,
          MODIFIED_ON,
          AGENT_ID,
          ACTION,
          DIFF_COLUMN,
          OLD_VALUE,
          NEW_VALUE
      )
      VALUES (
          :NEW.ID,
          :NEW.MEMBER_ID,
          :NEW.NAME,
          :NEW.TYPE_ID,
          :NEW.FILTER,
          :NEW.ITEMS_PER_PAGE,
          :NEW.SORT_BY,
          :NEW.SORT_DESC,
          :NEW.RESULTS_HEADERS,
          OPERATION_DATE,
          :NEW.AGENT_ID,
          'U',
          'RESULTS_HEADERS',
          TO_CHAR(:OLD.RESULTS_HEADERS),
          TO_CHAR(:NEW.RESULTS_HEADERS)
      );
    END IF;

  END IF;

  IF DELETING THEN
    INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH (
      ID,
      MEMBER_ID,
      NAME,
      TYPE_ID,
      FILTER,
      ITEMS_PER_PAGE,
      SORT_BY,
      SORT_DESC,
      RESULTS_HEADERS,
      MODIFIED_ON,
      AGENT_ID,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :OLD.ID,
      :OLD.MEMBER_ID,
      :OLD.NAME,
      :OLD.TYPE_ID,
      :OLD.FILTER,
      :OLD.ITEMS_PER_PAGE,
      :OLD.SORT_BY,
      :OLD.SORT_DESC,
      :OLD.RESULTS_HEADERS,
      OPERATION_DATE,
      :OLD.AGENT_ID,
      'D',
      NULL,
      NULL,
      NULL
    );
  END IF;

  IF INSERTING THEN
    INSERT INTO {{ HISTORY_SCHEMA_NAME }}.SE_SEARCH (
      ID,
      MEMBER_ID,
      NAME,
      TYPE_ID,
      FILTER,
      ITEMS_PER_PAGE,
      SORT_BY,
      SORT_DESC,
      RESULTS_HEADERS,
      MODIFIED_ON,
      AGENT_ID,
      ACTION,
      DIFF_COLUMN,
      OLD_VALUE,
      NEW_VALUE
    )
    VALUES (
      :NEW.ID,
      :NEW.MEMBER_ID,
      :NEW.NAME,
      :NEW.TYPE_ID,
      :NEW.FILTER,
      :NEW.ITEMS_PER_PAGE,
      :NEW.SORT_BY,
      :NEW.SORT_DESC,
      :NEW.RESULTS_HEADERS,
      OPERATION_DATE,
      :NEW.AGENT_ID,
      'I',
      NULL,
      NULL,
      NULL
    );
  END IF;
END;

/

INSERT INTO SE_SEARCH_TYPE (NAME, AGENT_ID, MODIFIED_ON)
VALUES ('surveys', 22625, SYSDATE);

COMMIT;

