<?php

namespace LHCb\RPSurvey\Search\Infrastructure\Web;

use Slim\App;

class SearchRoutes
{
    public static function addRoutes(App $app): void
    {
        $app->get("/members/{id}/searches", [SearchController::class, "findMemberSearches"]);
        $app->post("/searches", [SearchController::class, "saveSearch"]);
        $app->get("/searches/{id}", [SearchController::class, "findSearch"]);
        $app->delete("/searches/{id}", [SearchController::class, "deleteSearch"]);
    }
}
