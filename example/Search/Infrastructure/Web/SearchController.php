<?php

declare(strict_types=1);

namespace LHCb\RPSurvey\Search\Infrastructure\Web;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Throwable;
use Glance\ErrorMiddleware\BaseException;
use Glance\ErrorMiddleware\Error;
use LHCb\RPSurvey\User\Domain\UserRepositoryInterface;
use LHCb\Search\Infrastructure\Provider\SearchProvider;

final class SearchController
{
    private $searchProvider;
    private $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
        SearchProvider $searchProvider
    ) {
        $this->userRepository = $userRepository;
        $this->searchProvider = $searchProvider;
    }

    public function saveSearch(Request $request, Response $response): Response
    {
        $input = json_decode((string) $request->getBody(), true);
        $user = $request->getAttribute("keycloak-user");
        $agentId = $this->userRepository->findIdGivenPersonId(
            $user->personId()
        );

        try {
            $newSearchId = $this->searchProvider->saveSearch(
                $input["search"],
                $agentId
            );
        } catch (\InvalidArgumentException $e) {
            throw $this->toApiException($e);
        }

        $responseBody = json_encode(["search" => $this->searchProvider->getSearchConfigurationById($newSearchId)]);
        $response->getBody()->write($responseBody);
        return $response;
    }

    public function findSearch(Request $request, Response $response): Response
    {
        $searchId = (int) $request->getAttribute("id");
        $json = json_encode([
            "search" => $this->searchProvider->getSearchConfigurationById($searchId)
        ]);
        $response->getBody()->write($json);

        return $response;
    }

    public function findMemberSearches(Request $request, Response $response): Response
    {
        $memberId = (int) $request->getAttribute("id");
        $json = json_encode([
            "searches" => $this->searchProvider->getMemberSearchConfigurations($memberId)
        ]);
        $response->getBody()->write($json);

        return $response;
    }

    public function deleteSearch(Request $request, Response $response): Response
    {
        $searchId = (int) $request->getAttribute("id");

        try {
            $this->searchProvider->deleteSearch($searchId);
        } catch (\DomainException $e) {
            throw $this->toApiException($e);
        } catch (\InvalidArgumentException $e) {
            throw $this->toApiException($e);
        }
        /** Return a response with HTTP status code 204 (HTTP_NO_CONTENT) */
        return $response->withStatus(204);
    }

    private function toApiException(Throwable $throwable): BaseException
    {
        $error = new Error();
        $error->setStatus(400)
              ->setTitle($throwable->getMessage());

        $exception = new BaseException(400);
        $exception->addError($error);

        return $exception;
    }
}
