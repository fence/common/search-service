<?php

namespace LHCb\RPSurvey\Survey\Infrastructure\Web;

use Slim\App;

class SurveyRoutes
{
    public static function addRoutes(App $app): void
    {
        $app->get("/surveys/search", [SurveyController::class, "search"]);
    }
}
