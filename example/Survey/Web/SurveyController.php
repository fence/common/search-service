<?php

declare(strict_types=1);

namespace LHCb\RPSurvey\Survey\Infrastructure\Web;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Throwable;
use Glance\ErrorMiddleware\BaseException;
use Glance\ErrorMiddleware\Error;
use Glance\Search\Application\RunSearchWithFilters\SearchInputDTO;
use Glance\Search\Infrastructure\Provider\SearchProvider;

class SurveyController
{
    private $searchProvider;

    public function __construct(
        SearchProvider $searchProvider
    ) {
        $this->searchProvider = $searchProvider;
    }

    public function search(Request $request, Response $response): Response
    {
        $responseBody = null;

        $input = $request->getQueryParams();
        $searchParameters = new SearchInputDTO($input);

        try {
            $responseBody = json_encode(
                $this->searchProvider->runSearch(
                    $searchParameters,
                    __DIR__ . "/../../../../resources/search/surveys.json"
                )
            );
        } catch (\InvalidArgumentException $e) {
            throw $this->toApiException($e);
        }

        $response->getBody()->write($responseBody);
        return $response;
    }

    private function toApiException(Throwable $throwable): BaseException
    {
        $error = new Error();
        $error->setStatus(400)
              ->setTitle($throwable->getMessage());

        $exception = new BaseException(400);
        $exception->addError($error);

        return $exception;
    }
}
