WITH SURVEY_RESPONSIBLE AS (
    SELECT
        SR.SURVEY_ID,
        LISTAGG(m.NAME, '; ')
            WITHIN GROUP (ORDER BY m.NAME)
            AS RESPONSIBLE_PERSONS
    FROM RP_SURVEY_RESPONSIBLE SR
    INNER JOIN (
        SELECT ID,
        INITCAP(FIRST_NAME || ' ' || LAST_NAME) AS NAME
        FROM GL_USER_VIEW
    ) m
        ON m.id = SR.RESPONSIBLE_ID
    GROUP BY SR.SURVEY_ID
)
SELECT
    s.ID,
    s.DESCRIPTION,
    s.COMMENTS,
    s.MAGNET_ON,
    CASE MAGNET_ON WHEN 'Y' THEN 'Yes' WHEN 'N' THEN 'No' ELSE NULL END AS MAGNET_ON_AS_STRING,
    s.STARTED_ON,
    TO_CHAR(s.STARTED_ON,'YYYY-MM-DD HH24:MI') AS STARTED_ON_AS_STRING,
    s.BEAM_OFF_AT,
    TO_CHAR(s.BEAM_OFF_AT,'YYYY-MM-DD HH24:MI') AS BEAM_OFF_AT_AS_STRING,
    s.RAMSES_LOG_LINK,
    sr.RESPONSIBLE_PERSONS as RESPONSIBLE_PERSONS
    FROM RP_SURVEY s
    LEFT JOIN SURVEY_RESPONSIBLE sr ON sr.SURVEY_ID = s.id
    ORDER BY s.STARTED_ON DESC