<?php

declare(strict_types=1);

namespace Glance\Search\Application\RunSearchWithFilters;

/**
 * Search Input DTO
 */
class SearchInputDTO
{
    public $offset;

    public $limit;

    public $sortByField;

    public $sortDesc;

    public $lookupText;

    public $queryString;

    public function __construct(array $input)
    {
        if (isset($input["offset"])) {
            $this->offset = $input["offset"];
        }

        if (isset($input["limit"])) {
            $this->limit = $input["limit"];
        }

        if (isset($input["sortBy"])) {
            $this->sortByField = $input["sortBy"];
        }

        if (isset($input["sortDesc"])) {
            $this->sortDesc = $input["sortDesc"];
        }

        if (isset($input["lookup"])) {
            $this->lookupText = $input["lookup"];
        }

        if (isset($input["queryString"])) {
            $this->queryString = urldecode($input["queryString"]);
        }
    }
}
