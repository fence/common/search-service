<?php

declare(strict_types=1);

namespace Glance\Search\Application\RunSearchWithFilters;

use Glance\Search\Domain\SearchRepositoryInterface;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;
use Glance\Search\Domain\Filter;
use Glance\Search\Domain\SearchConfiguration;

class RunSearchHandler
{
    private const DEFAULT_PAGINATION_OFFSET = 0;
    private const DEFAULT_PAGINATION_LIMIT = 50;
    private $repository;

    public function __construct(
        SearchRepositoryInterface $repository
    ) {
        $this->repository = $repository;
    }

    /**
     * @param SearchInputDTO $searchParameters
     * @param string $searchConfigurationPath
     *
     * @return array
     */
    public function search(
        SearchInputDTO $searchParameters,
        string $searchConfigurationPath
    ): array {
        /** Search tools */
        $searchConfiguration = SearchConfiguration::fromPath($searchConfigurationPath);
        $query = $searchConfiguration->query();
        $cachePool = $cachePool = $searchConfiguration->cacheConfigurationProvided()
        ? $this->pool($searchConfiguration->cacheRepository(), $searchConfiguration->cacheFolder()) : null;
        $shouldUseCache = $cachePool !== null && $this->shouldUseCache(
            $searchParameters,
            $searchConfiguration->cacheOffset(),
            $searchConfiguration->cacheLimit(),
            $searchConfiguration->cacheQueryString(),
            $searchConfiguration->cacheLookup()
        );

        /** Search parameters */
        $lookupText = $searchParameters->lookupText ?? '';
        $sortBy = $searchParameters->sortByField ?? null;
        $sortBy = $sortBy ? $searchConfiguration->getColumn($sortBy) : null;
        $sortDesc = $searchParameters->sortDesc  ?? null;
        $sortDesc = $sortDesc ? $sortDesc === "true" : null;
        $queryString = isset($searchParameters->queryString) ? $searchParameters->queryString : null;
        $paginationOffset = isset($searchParameters->offset)
            ? (int) $searchParameters->offset : self::DEFAULT_PAGINATION_OFFSET;
        $paginationLimit = isset($searchParameters->limit)
            ? (int) $searchParameters->limit : self::DEFAULT_PAGINATION_LIMIT;

        /**
         * Tries to get search results from cache
         */
        if ($shouldUseCache) {
            $item = $cachePool->getItem($searchConfiguration->cacheKey());
            if ($item->get()) {
                return $item->get();
            }
        }

        /**
         * When a query string is given, it will be translated into a SQL filter
         */
        if ($queryString) {
            $filter = Filter::create($queryString, $searchConfiguration);
            $query = "SELECT * FROM ( $query ) " . $filter->toSqlString();
        }

        $results = $this->repository->runSearch(
            $query,
            $paginationOffset,
            $paginationLimit,
            $lookupText,
            $sortBy,
            $sortDesc,
            $searchConfiguration->mainTablePK(),
            $searchConfiguration->lookupTablePK(),
            $searchConfiguration->lookupTable(),
        );

        /**
         * Formats query results
         *
         * Eg.: $results [["NAME" => "Gabriel", "INSTITUTE_ID" => 2]]
         *
         * becomes
         *
         * $formattedResults = [["name" => "Gabriel", "instituteId" => 2]]
         */
        $formattedResults = [];
        foreach ($results["results"] as $result) {
            $formattedResult = [];
            foreach ($result as $key => $value) {
                $formattedResult[$searchConfiguration->getFieldFromColumn($key) ?? $key] = $value;
            }
            $formattedResults[] = $formattedResult;
        }

        $results["results"] = $formattedResults;

        /**
         * Saves search result in case cache configuration is defined
         */
        if ($shouldUseCache) {
            $item = $cachePool->getItem($searchConfiguration->cacheKey());
            $item->set($results);
            $cachePool->save($item);
        }

        return $results;
    }

    private function pool(string $repository, string $folder)
    {
        $filesystemAdapter = new Local($repository . "/");
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder($folder);
        return $pool;
    }

    public function purgeSearchCache(string $searchConfigurationPath)
    {
        $searchConfiguration = $this->getConfiguration($searchConfigurationPath);
        $cacheConfiguration = isset($searchConfiguration["cache"]) ? $searchConfiguration["cache"] : null;

        if (!$cacheConfiguration) {
            throw new \Exception("Missing cache configuration");
        }

        $pool = $this->pool($cacheConfiguration["repository"], $cacheConfiguration["folder"]);
        $pool->deleteItem($cacheConfiguration["key"]);
    }

    private function shouldUseCache(
        SearchInputDTO $input,
        $cacheOffset,
        $cacheLimit,
        $cacheQueryString,
        $cacheLookup
    ): bool {
        return (($input->offset ?? self::DEFAULT_PAGINATION_OFFSET) == $cacheOffset)
            && (($input->limit ?? self::DEFAULT_PAGINATION_LIMIT) == $cacheLimit)
            && (($input->queryString ?? "") == $cacheQueryString)
            && (($input->lookupText ?? "") == $cacheLookup);
    }
}
