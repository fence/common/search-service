<?php

declare(strict_types=1);

namespace Glance\Search\Application\SaveSearch;

use InvalidArgumentException;

class SaveSearchCommand
{
    private $memberId;
    private $name;
    private $typeId;
    private $filter;
    private $itemsPerPage;
    private $sortBy;
    private $sortDesc;
    private $resultsHeaders;

    public function __construct(array $input)
    {
        if (!isset($input["member"]["id"])) {
            throw new InvalidArgumentException("Missing member ID");
        }

        if (!isset($input["name"])) {
            throw new InvalidArgumentException("Missing search name");
        }

        if (!isset($input["itemsPerPage"])) {
            throw new InvalidArgumentException("Missing search itemsPerPage");
        }

        if (!isset($input["type"]["id"])) {
            throw new InvalidArgumentException("Missing type ID");
        }

        if (!isset($input["resultsHeaders"])) {
            throw new InvalidArgumentException("Missing results header");
        }

        $this->memberId = (int) $input["member"]["id"];
        $this->name = $input["name"];
        $this->typeId = (int) $input["type"]["id"];
        $this->itemsPerPage = (int) $input["itemsPerPage"];
        $this->filter = $input["filter"] ?? null;
        $this->sortBy = $input["sortBy"] ?? null;
        $this->sortDesc = $input["sortDesc"] ?? null;
        $this->resultsHeaders = $input["resultsHeaders"];
    }

    public function memberId(): int
    {
        return $this->memberId;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function itemsPerPage(): int
    {
        return $this->itemsPerPage;
    }

    public function typeId(): int
    {
        return $this->typeId;
    }

    public function filter(): ?string
    {
        return $this->filter;
    }

    public function sortBy(): ?string
    {
        return $this->sortBy;
    }

    public function sortDesc(): ?string
    {
        return $this->sortDesc;
    }

    public function resultsHeaders(): string
    {
        return $this->resultsHeaders;
    }
}
