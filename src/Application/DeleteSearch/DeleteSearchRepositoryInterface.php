<?php

declare(strict_types=1);

namespace Glance\Search\Application\DeleteSearch;

interface DeleteSearchRepositoryInterface
{
    public function deleteSearch(int $searchId): void;
}
