<?php

declare(strict_types=1);

namespace Glance\Search\Application\GetSearchDetails;

interface SearchViewRepositoryInterface
{
    public function getSearchConfigurationById(int $searchId);

    public function getMemberSearchConfigurations(int $memberId);
}
