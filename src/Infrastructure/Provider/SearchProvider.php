<?php

declare(strict_types=1);

namespace Glance\Search\Infrastructure\Provider;

use Glance\Search\Application\DeleteSearch\DeleteSearchRepositoryInterface;
use Glance\Search\Application\GetSearchDetails\SearchViewRepositoryInterface;
use Glance\Search\Application\RunSearchWithFilters\RunSearchHandler;
use Glance\Search\Application\RunSearchWithFilters\SearchInputDTO;
use Glance\Search\Application\SaveSearch\SaveSearchCommand;
use Glance\Search\Domain\SearchConfiguration;
use Glance\Search\Domain\SearchRepositoryInterface;

class SearchProvider
{
    private $searchRepository;
    private $runSearchHandler;
    private $searchViewRepository;
    private $deleteSearchRepository;

    public function __construct(
        SearchRepositoryInterface $searchRepository,
        RunSearchHandler $runSearchHandler,
        SearchViewRepositoryInterface $searchViewRepository,
        DeleteSearchRepositoryInterface $deleteSearchRepository
    ) {
        $this->searchRepository = $searchRepository;
        $this->runSearchHandler = $runSearchHandler;
        $this->searchViewRepository = $searchViewRepository;
        $this->deleteSearchRepository = $deleteSearchRepository;
    }

    public function runSearch(
        SearchInputDTO $command,
        string $searchConfigurationPath
    ): array {
        return $this->runSearchHandler->search($command, $searchConfigurationPath);
    }

    public function getSearchConfigurationById(int $searchId): array
    {
        return $this->searchViewRepository->getSearchConfigurationById($searchId);
    }

    public function getMemberSearchConfigurations(int $memberId): array
    {
        return $this->searchViewRepository->getMemberSearchConfigurations($memberId);
    }

    public function deleteSearch(int $searchId): void
    {
        $this->deleteSearchRepository->deleteSearch($searchId);
    }

    public function saveSearch(array $input, int $agentId): int
    {
        return $this->searchRepository->saveSearch(
            new SaveSearchCommand($input),
            $agentId
        );
    }

    public function purgeSearchCache(string $searchConfigurationPath): void
    {
        $this->searchRepository->purgeSearchCache(SearchConfiguration::fromPath($searchConfigurationPath));
    }
}
