<?php

namespace Glance\Search\Infrastructure\Provider;

use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Driver\OCI8\OCI8Connection;
use DI\ContainerBuilder;
use Glance\Search\Application\DeleteSearch\DeleteSearchRepositoryInterface;
use Glance\Search\Application\GetSearchDetails\SearchViewRepositoryInterface;
use Glance\Search\Application\RunSearchWithFilters\RunSearchHandler;
use Glance\Search\Domain\SearchRepositoryInterface;
use Glance\Search\Infrastructure\Persistence\SqlSearchRepository;

final class SearchProviderFactory
{
    public static function getInstance(
        string $databaseUsername,
        string $databasePassword,
        string $databaseDns
    ): SearchProvider {
        $definitions = [
            Connection::class => function () use ($databaseUsername, $databasePassword, $databaseDns) {
                $connection = new OCI8Connection($databaseUsername, $databasePassword, $databaseDns);
                return $connection;
            },
            DeleteSearchRepositoryInterface::class => function (Connection $conn) {
                return new SqlSearchRepository($conn);
            },
            SearchViewRepositoryInterface::class => function (Connection $conn) {
                return new SqlSearchRepository($conn);
            },
            SearchRepositoryInterface::class => function (Connection $conn) {
                return new SqlSearchRepository($conn);
            },
        ];

        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions($definitions);
        $container = $containerBuilder->build();
        return new SearchProvider(
            $container->get(SearchRepositoryInterface::class),
            $container->get(RunSearchHandler::class),
            $container->get(SearchViewRepositoryInterface::class),
            $container->get(DeleteSearchRepositoryInterface::class),
        );
    }
}
