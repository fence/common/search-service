<?php

declare(strict_types=1);

namespace Glance\Search\Infrastructure\Persistence;

use Doctrine\DBAL\Driver\Connection;
use Glance\Search\Application\DeleteSearch\DeleteSearchRepositoryInterface;
use Glance\Search\Application\GetSearchDetails\SearchViewRepositoryInterface;
use Glance\Search\Application\SaveSearch\SaveSearchCommand;
use Glance\Search\Domain\SearchConfiguration;
use Glance\Search\Domain\SearchRepositoryInterface;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;

class SqlSearchRepository implements
    SearchRepositoryInterface,
    SearchViewRepositoryInterface,
    DeleteSearchRepositoryInterface
{
    private $connection;

    public function __construct(
        Connection $connection
    ) {
        $this->connection = $connection;
    }

    public function runSearch(
        string $query,
        int $offset,
        int $limit,
        ?string $lookupText,
        ?string $sortBy,
        ?bool $sortDesc,
        ?string $lookupMainTablePk = null,
        ?string $lookupTablePk = null,
        ?string $lookupTableName = null
    ) {
        $completeQuery = "
        SELECT * FROM ( " . $query . " )
        ";

        /**
         * Adds the lookup parameter in case it exists
        */
        if ($this->lookupConfigurationIsComplete($lookupText, $lookupMainTablePk, $lookupTablePk, $lookupTableName)) {
            $completeQuery = "
            SELECT * FROM ( " . $query . " )
            WHERE $lookupMainTablePk IN (
                SELECT $lookupTablePk
                from $lookupTableName
                WHERE UPPER(lookup) LIKE '%' || UPPER(:varLookupText) || '%'
            )
            ";
        }

        /**
         * Adds the order by parameter
         */
        if ($sortBy !== null && $sortDesc !== null) {
            $desc = ($sortDesc) ? "DESC" : "ASC";
            $completeQuery = $completeQuery . " ORDER BY $sortBy $desc ";
        }

        /**
         * Adds pagination
         */
        $completeQuery = $completeQuery . "OFFSET :varResultsOffset ROWS FETCH NEXT :varResultsLimit ROWS ONLY";

        $countResultsQuery = "
        SELECT COUNT(*) AS NUMBER_OF_RESULTS FROM ( " . $query . " )
        ";

        $paginationBinds = ["varResultsOffset" => $offset, "varResultsLimit" => $limit, "varLookupText" => $lookupText];
        $statement = $this->connection->prepare($completeQuery);
        $statement->execute($this->getUsedPaginationBinds($completeQuery, $paginationBinds));
        $results = $statement->fetchAllAssociative();

        $statement = $this->connection->prepare($countResultsQuery);
        $statement->execute($this->getUsedPaginationBinds($countResultsQuery, $paginationBinds));
        $numberOfResults = $statement->fetchAllAssociative()[0]["NUMBER_OF_RESULTS"];

        return [
            "results" => $results,
            "numberOfResults" => $numberOfResults
        ];
    }

    private function getUsedPaginationBinds($query, $binds): array
    {
        $finalPaginationBinds = [];
        foreach ($binds as $key => $value) {
            if (strpos($query, $key) !== false) {
                $finalPaginationBinds[$key] = $value;
            }
        }
        return $finalPaginationBinds;
    }

    public function findNextSearchId(): int
    {
        $query = "SELECT DATA_DEFAULT AS SEQUENCE FROM USER_TAB_COLS
        WHERE  TABLE_NAME = 'SE_SEARCH' AND COLUMN_NAME = 'ID'";
        $statement = $this->connection->prepare($query);
        $statement->execute();
        $rows = $statement->fetchAll();
        $sequenceName = $rows[0]["SEQUENCE"];

        $query = "SELECT $sequenceName AS NEXT_ID FROM DUAL";
        $statement = $this->connection->prepare($query);
        $statement->execute();
        $rows = $statement->fetchAll();
        return (int) $rows[0]["NEXT_ID"];
    }

    public function saveSearch(
        SaveSearchCommand $command,
        int $agentId
    ): int {
        $newId = $this->findNextSearchId();
        $query = "INSERT INTO SE_SEARCH (
            ID,
            MEMBER_ID,
            NAME,
            TYPE_ID,
            FILTER,
            ITEMS_PER_PAGE,
            SORT_BY,
            SORT_DESC,
            RESULTS_HEADERS,
            AGENT_ID,
            MODIFIED_ON
        )
        VALUES(
            :varId,
            :varMemberId,
            :varName,
            :varTypeId,
            :varFilter,
            :varItemsPerPage,
            :varSortBy,
            :varSortDesc,
            :varResultsHeaders,
            :varAgentId,
            SYSDATE
        )";

        $binds = [
            "varId" => $newId,
            "varMemberId" => $command->memberId(),
            "varName" => $command->name(),
            "varTypeId" => $command->typeId(),
            "varFilter" => $command->filter(),
            "varItemsPerPage" => $command->itemsPerPage(),
            "varSortBy" => $command->sortBy(),
            "varSortDesc" => $command->sortDesc(),
            "varResultsHeaders" => $command->resultsHeaders(),
            "varAgentId" => $agentId
        ];
        $statement = $this->connection->prepare($query);
        $statement->execute($binds);
        $this->connection->commit();
        return $newId;
    }

    public function getSearchConfigurationById(int $searchId)
    {
        $query = "
            SELECT
                se.ID,
                se.MEMBER_ID,
                se.NAME,
                se.TYPE_ID,
                t.NAME AS TYPE_NAME,
                se.FILTER,
                se.ITEMS_PER_PAGE,
                se.SORT_BY,
                se.SORT_DESC,
                se.RESULTS_HEADERS,
                se.MODIFIED_ON
            FROM SE_SEARCH se
            INNER JOIN SE_SEARCH_TYPE t ON se.TYPE_ID = t.ID
            WHERE se.ID = :varId
        ";
        $statement = $this->connection->prepare($query);
        $statement->execute(["varId" => $searchId]);
        $searchRow = $statement->fetchAll()[0];

        return [
            "id" => (int) $searchRow["ID"],
            "member" => [
                "id" => (int) $searchRow["MEMBER_ID"],
            ],
            "name" => $searchRow["NAME"],
            "type" => [
                "id" => (int) $searchRow["TYPE_ID"],
                "name" => $searchRow["TYPE_NAME"]
            ],
            "filter" => $searchRow["FILTER"],
            "itemsPerPage" => $searchRow["ITEMS_PER_PAGE"],
            "sortBy" => $searchRow["SORT_BY"],
            "sortDesc" => $searchRow["SORT_DESC"],
            "resultsHeaders" => $searchRow["RESULTS_HEADERS"],
            "modifiedOn" => $searchRow["MODIFIED_ON"]
        ];
    }

    public function getMemberSearchConfigurations(int $memberId)
    {
        $query = "
            SELECT
                se.ID,
                se.MEMBER_ID,
                se.NAME,
                se.TYPE_ID,
                t.NAME AS TYPE_NAME,
                se.FILTER,
                se.ITEMS_PER_PAGE,
                se.SORT_BY,
                se.SORT_DESC,
                se.RESULTS_HEADERS,
                se.MODIFIED_ON
            FROM SE_SEARCH se
            INNER JOIN SE_SEARCH_TYPE t ON se.TYPE_ID = t.ID
            WHERE se.MEMBER_ID = :varMemberId
        ";
        $statement = $this->connection->prepare($query);
        $statement->execute(["varMemberId" => $memberId]);
        return array_map(function (array $searchRow) {
            return [
                "id" => (int) $searchRow["ID"],
                "member" => [
                    "id" => (int) $searchRow["MEMBER_ID"],
                ],
                "name" => $searchRow["NAME"],
                "type" => [
                    "id" => (int) $searchRow["TYPE_ID"],
                    "name" => $searchRow["TYPE_NAME"]
                ],
                "filter" => $searchRow["FILTER"],
                "itemsPerPage" => $searchRow["ITEMS_PER_PAGE"],
                "sortBy" => $searchRow["SORT_BY"],
                "sortDesc" => $searchRow["SORT_DESC"],
                "resultsHeaders" => $searchRow["RESULTS_HEADERS"],
                "modifiedOn" => $searchRow["MODIFIED_ON"]
            ];
        }, $statement->fetchAll());
    }

    public function deleteSearch(int $searchId): void
    {
        $query = "DELETE FROM SE_SEARCH WHERE ID = :varSearchId";
        $statement = $this->connection->prepare($query);
        $statement->execute(["varSearchId" => $searchId]);
        $this->connection->commit();
    }

    private function lookupConfigurationIsComplete(
        ?string $lookupMainTablePk = null,
        ?string $lookupTablePk = null,
        ?string $lookupTableName = null
    ): bool {
        return $lookupMainTablePk
        && $lookupTablePk
        && $lookupTableName;
    }

    public function purgeSearchCache(SearchConfiguration $searchConfiguration): void
    {
        if (!$searchConfiguration->cacheConfigurationProvided()) {
            return;
        }

        $filesystemAdapter = new Local($searchConfiguration->cacheRepository() . "/");
        $filesystem = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        $pool->setFolder($searchConfiguration->cacheFolder());
        $pool->deleteItem($searchConfiguration->cacheKey());
    }
}
