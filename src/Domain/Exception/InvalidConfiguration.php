<?php

declare(strict_types=1);

namespace Glance\Search\Domain\Exception;

/**
 * InvalidConfiguration is thrown when the search configuration file is invalid
 */
class InvalidConfiguration extends \Exception
{
}
