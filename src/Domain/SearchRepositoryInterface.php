<?php

declare(strict_types=1);

namespace Glance\Search\Domain;

use Glance\Search\Application\SaveSearch\SaveSearchCommand;

interface SearchRepositoryInterface
{
    public function runSearch(
        string $query,
        int $offset,
        int $limit,
        ?string $lookupText,
        ?string $sortBy,
        ?bool $sortDesc,
        ?string $lookupMainTablePk = null,
        ?string $lookupTablePk = null,
        ?string $lookupTableName = null
    );

    public function saveSearch(
        SaveSearchCommand $command,
        int $agentId
    ): int;

    public function purgeSearchCache(SearchConfiguration $searchConfiguration): void;
}
