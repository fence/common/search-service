<?php

declare(strict_types=1);

namespace Glance\Search\Domain;

class Field
{
    private $columnName;
    private $type;
    private $format;

    private const DATE = "date";
    private const DEFAULT_FORMAT = "YYYY-MM-DD";

    public function __construct(
        string $columnName,
        ?string $type = null,
        ?string $format = null
    ) {
        $this->columnName = $columnName;
        $this->type = $type;
        $this->format = $format;
    }

    public function toSqlString(): string
    {
        $columnName = $this->columnName;
        $format = $this->format();

        return $this->type === self::DATE
        ? "TO_DATE(TO_CHAR(CAST($columnName as date), '$format'), '$format')"
        : "UPPER($columnName)";
    }

    public function toString(): ?string
    {
        return $this->columnName;
    }

    public function format(): string
    {
        return $this->format ?? self::DEFAULT_FORMAT;
    }
}
