<?php

declare(strict_types=1);

namespace Glance\Search\Domain;

use Glance\Search\Domain\Exception\InvalidConfiguration;

class SearchConfiguration
{
    private $queryPath;
    private $properties;
    private $cacheConfiguration = null;
    private $lookupConfiguration = null;
    private $columnToFieldMap = [];
    private const DEFAULT_CACHE_OFFSET = 0;
    private const DEFAULT_CACHE_LIMIT = 50;

    public function __construct(
        string $queryPath,
        array $properties,
        ?array $lookupConfiguration,
        ?array $cacheConfiguration
    ) {
        $this->queryPath = $queryPath;
        $this->assignProperties($properties);
        $this->assignLookupConfiguration($lookupConfiguration);
        $this->assignCacheConfiguration($cacheConfiguration);
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data["query"],
            $data["properties"],
            $data["lookup"] ?? null,
            $data["cache"] ?? null
        );
    }

    public static function fromPath(string $path): self
    {
        if (!file_exists($path)) {
            throw new InvalidConfiguration("Search configuration path is invalid. Path given: $path");
        }

        return SearchConfiguration::fromArray(json_decode(file_get_contents($path), true));
    }

    public function assignLookupConfiguration(?array $lookupConfiguration): void
    {
        if ($lookupConfiguration === null) {
            return;
        }

        if (!array_key_exists("lookupTable", $lookupConfiguration)) {
            throw new InvalidConfiguration("Missing lookupTable");
        }

        if (!array_key_exists("lookupTablePK", $lookupConfiguration)) {
            throw new InvalidConfiguration("Missing lookupTablePK");
        }

        if (!array_key_exists("mainTablePK", $lookupConfiguration)) {
            throw new InvalidConfiguration("Missing mainTablePK");
        }

        $this->lookupConfiguration = $lookupConfiguration;
    }

    public function lookupTable(): string
    {
        return $this->lookupConfiguration["lookupTable"];
    }

    public function lookupTablePK(): string
    {
        return $this->lookupConfiguration["lookupTablePK"];
    }

    public function mainTablePK(): string
    {
        return $this->lookupConfiguration["mainTablePK"];
    }

    public function assignCacheConfiguration(?array $cacheConfiguration): void
    {
        if ($cacheConfiguration === null) {
            return;
        }

        if (!array_key_exists("key", $cacheConfiguration)) {
            throw new InvalidConfiguration("Missing key");
        }

        if (!array_key_exists("repository", $cacheConfiguration)) {
            throw new InvalidConfiguration("Missing repository");
        }

        if (!array_key_exists("folder", $cacheConfiguration)) {
            throw new InvalidConfiguration("Missing folder");
        }

        $this->cacheConfiguration = $cacheConfiguration;
    }

    public function cacheKey(): string
    {
        return $this->cacheConfiguration["key"];
    }

    public function cacheRepository(): string
    {
        return $this->cacheConfiguration["repository"];
    }

    public function cacheFolder(): string
    {
        return $this->cacheConfiguration["folder"];
    }

    public function cacheQueryString(): ?string
    {
        return $this->cacheConfiguration["queryString"] ?? null;
    }

    public function cacheLookup(): ?string
    {
        return $this->cacheConfiguration["lookup"] ?? null;
    }

    public function cacheOffset(): int
    {
        return $this->cacheConfiguration["offset"]
            ? (int) $this->cacheConfiguration["offset"] : self::DEFAULT_CACHE_OFFSET;
    }

    public function cacheLimit(): int
    {
        return $this->cacheConfiguration["limit"]
            ? (int) $this->cacheConfiguration["limit"] : self::DEFAULT_CACHE_LIMIT;
    }

    public function assignProperties(array $properties): void
    {
        foreach ($properties as $key => $value) {
            if (!isset($properties[$key]["compatibleOperators"])) {
                throw new InvalidConfiguration("Missing compatibleOperators for property $key");
            }

            if (!isset($properties[$key]["column"])) {
                throw new InvalidConfiguration("Missing column for property $key");
            }

            $customMatches = $properties[$key]["customMatch"] ?? [];
            foreach ($customMatches as $customMatch) {
                if (!isset($customMatch["operator"])) {
                    throw new InvalidConfiguration("Missing operator for customMatch for property $key");
                }

                if (!isset($customMatch["match"])) {
                    throw new InvalidConfiguration("Missing match for customMatch for property $key");
                }
            }

            $this->columnToFieldMap[$properties[$key]["column"]] = $key;
        }

        $this->properties = $properties;
    }

    public function getCompatibleOperators(string $property): ?array
    {
        return $this->properties[$property]["compatibleOperators"];
    }

    public function getColumn(string $property): ?string
    {
        return $this->properties[$property]["column"];
    }

    public function getType(string $property): ?string
    {
        return $this->properties[$property]["type"] ?? null;
    }

    public function getFormat(string $property): ?string
    {
        return $this->properties[$property]["format"] ?? null;
    }

    public function getCustomMatch(string $property, string $operator): ?string
    {
        $customMatches = $this->properties[$property]["customMatch"] ?? [];
        foreach ($customMatches as $customMatch) {
            if ($customMatch["operator"] === $operator) {
                return $customMatch["match"];
            }
        }
        return null;
    }

    public function queryPath(): string
    {
        return $this->queryPath;
    }

    public function query(): string
    {
        $queryPath = getenv("API_FOLDER") . "/" . $this->queryPath();
        if (!file_exists($queryPath)) {
            throw new \Exception("Query path is invalid. Path given: $queryPath");
        }

        return file_get_contents($queryPath);
    }

    public function lookupConfigurationProvided(): bool
    {
        return $this->lookupConfiguration !== null;
    }

    public function cacheConfigurationProvided(): bool
    {
        return $this->cacheConfiguration !== null;
    }

    public function getFieldFromColumn(string $column): ?string
    {
        return $this->columnToFieldMap[$column] ?? null;
    }
}
