<?php

declare(strict_types=1);

namespace Glance\Search\Domain;

class GroupingMark
{
    private $value;
    public const OPEN = "(";
    public const CLOSE = ")";

    public function __construct(
        string $value
    ) {
        if (!self::isGroupingMark($value)) {
            throw new \InvalidArgumentException("Invalid grouping mark: $value");
        }

        $this->value = $value;
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    public static function isGroupingMark(string $value): bool
    {
        return $value === self::OPEN || $value === self::CLOSE;
    }

    public function toSqlString(bool $like = false): string
    {
        return $this->value;
    }
}
