<?php

declare(strict_types=1);

namespace Glance\Search\Domain;

class Value
{
    private $value;
    private $type;
    private $format;

    private const DATE = "date";
    private const FLOAT = "float";
    private const INT = "int";
    private const DEFAULT_FORMAT = "YYYY-MM-DD";
    private const STRING = "string";

    public const NULL = "NULL";
    public const NOT_NULL = "NOT NULL";
    public const EMPTY = "__EMPTY__";

    public function __construct(
        ?string $value,
        ?string $type = null,
        ?string $format = null
    ) {
        $this->value = $value === self::EMPTY ? null : $value;
        $this->type = $type;
        $this->format = $format;
    }

    public function toSqlString(bool $like = false)
    {
        $value = $like ? "%$this->value%" : $this->value;
        if ($value === null) {
            return "NULL";
        }
        $format = $this->format();

        $map = [
            self::DATE => [$this, 'convertDate'],
            self::FLOAT => [$this, 'convertFloat'],
            self::INT => [$this, 'convertInt'],
            self::STRING => [$this, 'convertString'],
            null => [$this, 'convertString'],
        ];

        return $map[$this->type]($value, $format);
    }

    private function convertString(string $value, string $format): string
    {
        return "UPPER('$value')";
    }

    private function convertDate(string $value, string $format): string
    {
        return "TO_DATE('$value', '$format')";
    }

    private function convertInt(string $value, string $format): int
    {
        return intval($value);
    }

    private function convertFloat(string $value, string $format): float
    {
        return floatval($value);
    }

    public function toString(): ?string
    {
        return $this->value;
    }

    public function isNull(): bool
    {
        return $this->value === null;
    }

    public function format(): string
    {
        return $this->format ?? self::DEFAULT_FORMAT;
    }
}
