<?php

declare(strict_types=1);

namespace Glance\Search\Domain;

class Conjunction
{
    private $value;
    public const AND = "AND";
    public const OR = "OR";

    public function __construct(
        string $value
    ) {
        $value = strtoupper($value);
        if (!self::isConjunction($value)) {
            throw new \InvalidArgumentException("Invalid conjunction: $value");
        }

        $this->value = $value;
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    public static function isConjunction(string $value): bool
    {
        return $value === self::AND || $value === self::OR;
    }

    public function toSqlString(): string
    {
        return $this->value;
    }
}
