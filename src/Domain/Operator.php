<?php

declare(strict_types=1);

namespace Glance\Search\Domain;

/**
 * Operator is the class capable of compiling a statement
 */
class Operator
{
    public const EQUAL = "=";
    public const EQUAL_OR_GREATER = ">=";
    public const EQUAL_OR_LESS = "<=";
    public const GREATER = ">";
    public const LESS = "<";
    public const CONTAIN = "contain";
    public const NOT_CONTAIN = "not-contain";
    public const NOT_EQUAL = "!=";
    public const AVAILABLE_OPERATORS = [
        self::EQUAL,
        self::EQUAL_OR_GREATER,
        self::EQUAL_OR_LESS,
        self::GREATER,
        self::LESS,
        self::CONTAIN,
        self::NOT_CONTAIN,
        self::NOT_EQUAL
    ];

    public const SQL_EQUIVALENT = [
        self::EQUAL => self::EQUAL,
        self::EQUAL_OR_GREATER => self::EQUAL_OR_GREATER,
        self::EQUAL_OR_LESS => self::EQUAL_OR_LESS,
        self::GREATER => self::GREATER,
        self::LESS => self::LESS,
        self::CONTAIN => "LIKE",
        self::NOT_CONTAIN => "NOT LIKE",
        self::NOT_EQUAL => self::NOT_EQUAL
    ];

    private $name;

    public function __construct(string $operatorName)
    {
        if (!in_array($operatorName, self::AVAILABLE_OPERATORS)) {
            throw new \InvalidArgumentException("Operator not supported");
        }

        $this->name = $operatorName;
    }

    public static function isOperatorString(string $word): bool
    {
        $availableOperators = [
            self::EQUAL,
            self::EQUAL_OR_GREATER,
            self::EQUAL_OR_LESS,
            self::GREATER,
            self::LESS,
            self::CONTAIN,
            self::NOT_CONTAIN,
            self::NOT_EQUAL
        ];
        return in_array($word, $availableOperators);
    }

    public function toSqlString(bool $valueIsEmpty = false): string
    {
        if ($valueIsEmpty) {
            $emptyMap = [
                self::EQUAL => "IS",
                self::NOT_EQUAL => "IS NOT",
            ];
            return $emptyMap[$this->name];
        }
        return self::SQL_EQUIVALENT[$this->name];
    }

    public function toString(): string
    {
        return $this->name;
    }

    public function isLike(): bool
    {
        return $this->name === self::CONTAIN || $this->name === self::NOT_CONTAIN;
    }
}
