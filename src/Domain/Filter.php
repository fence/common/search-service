<?php

declare(strict_types=1);

namespace Glance\Search\Domain;

use InvalidArgumentException;

/**
 * Class that knows how to divide the input query string into smaller parts. These parts can be
 * Statements, grouping marks or conjunctions.
 *
 * Statements are composed by a Field + an Operator + a value
 * Grouping marks are ")" or "("
 * Conjunctions are "AND" or "OR"
 *
 * When making null comparisons, the __EMPTY__ string should be used. For example:
 *  (responsibleName = Gabriel OR responsibleName = __EMPTY__)
 *
 * Fields and Values should be surrounded by "
 */
class Filter
{
    private $queryString;
    private $searchConfiguration;

    private const SPACE_ENCODING = "__";

    public function __construct(
        string $queryString,
        SearchConfiguration $searchConfiguration
    ) {
        $this->queryString = $queryString;
        $this->searchConfiguration = $searchConfiguration;
    }

    public static function create(
        string $queryString,
        SearchConfiguration $searchConfiguration
    ): self {
        return new self($queryString, $searchConfiguration);
    }

    /**
     * Compiles the query string to a SQL filter
     */
    public function toSqlString(): string
    {
        /*
         * Eg.: $encodedQueryIndividualWordsAsArray = ["responsibleName", "contain", "Gabriel_Jose"];
        */
        $encodedQueryIndividualWordsAsArray = explode(" ", $this->encodeFieldAndValueSpaces($this->queryString));
        $compiledQuery = [];
        $counter = 0;
        foreach ($encodedQueryIndividualWordsAsArray as $queryComponent) {
            if (Operator::isOperatorString($queryComponent)) {
                if (!isset($encodedQueryIndividualWordsAsArray[$counter - 1])) {
                    throw new InvalidArgumentException(
                        "Invalid query: missing field"
                    );
                }
                if (!isset($encodedQueryIndividualWordsAsArray[$counter + 1])) {
                    throw new InvalidArgumentException(
                        "Invalid query: missing value"
                    );
                }

                $field = $encodedQueryIndividualWordsAsArray[$counter - 1];
                $operator = $encodedQueryIndividualWordsAsArray[$counter];
                $value = $encodedQueryIndividualWordsAsArray[$counter + 1];

                $compiledQuery[] = Statement::create(
                    $this->searchConfiguration->getColumn($field),
                    $operator,
                    $value,
                    $this->searchConfiguration->getType($field),
                    $this->searchConfiguration->getFormat($field),
                    $this->searchConfiguration->getCustomMatch($field, $operator)
                )->toSqlString();
            }

            if (GroupingMark::isGroupingMark($queryComponent)) {
                $compiledQuery[] = GroupingMark::fromString($queryComponent)->toSqlString();
            }

            if (Conjunction::isConjunction($queryComponent)) {
                $compiledQuery[] = Conjunction::fromString($queryComponent)->toSqlString();
            }

            $counter++;
        }

        $compiledStatements = str_replace(self::SPACE_ENCODING, " ", join(" ", $compiledQuery));
        return $compiledStatements ? ("WHERE ($compiledStatements)") : '';
    }

    /**
     * Encodes Field's and Value's spaces
     *
     * Eg.:
     * input:  "responsible institutes" contain "Universidade de Santiago de Compostela";
     * output: "responsible__institutes" contain "Universidade__de__Santiago__de__Compostela";
     */
    private function encodeFieldAndValueSpaces(string $rawQuery)
    {
        $rawQueryAsArray = str_split($rawQuery);

        for ($i = 0; $i < count($rawQueryAsArray); $i++) {
            if ($rawQueryAsArray[$i] == '"') {
                $i++; // skips the current "
                while ($rawQueryAsArray[$i] != '"') {
                    if ($rawQueryAsArray[$i] == " ") {
                        $rawQueryAsArray[$i] = self::SPACE_ENCODING;
                    }
                    $i++;
                }
            }
        }

        $encodedQuery = join("", $rawQueryAsArray);
        $encodedQuery = str_replace('"', " ", $encodedQuery);
        $encodedQuery = preg_replace("!\s+!", " ", $encodedQuery);

        return $encodedQuery;
    }
}
