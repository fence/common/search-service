<?php

declare(strict_types=1);

namespace Glance\Search\Domain;

class Statement
{
    private $field;
    private $operator;
    private $value;
    private $customMatch;

    public function __construct(
        Field $field,
        Operator $operator,
        Value $value,
        ?string $customMatch = null
    ) {
        $this->field = $field;
        $this->operator = $operator;
        $this->value = $value;
        $this->customMatch = $customMatch;
    }

    public static function create(
        string $fieldName,
        string $operatorName,
        ?string $value,
        ?string $type = null,
        ?string $format = null,
        ?string $customMatch = null
    ): self {
        return new self(
            new Field($fieldName, $type, $format),
            new Operator($operatorName),
            new Value($value, $type, $format),
            $customMatch
        );
    }

    private function compileUsingCustomMatch(
        string $customMatch
    ) {
        return str_replace("__VALUE__", $this->value->toString(), $customMatch);
    }

    public function toSqlString(): string
    {
        if ($this->customMatch) {
            return $this->compileUsingCustomMatch($this->customMatch);
        }
        return $this->field->toSqlString() . " "
        . $this->operator->toSqlString($this->value->isNull()) . " "
        . $this->value->toSqlString($this->operator->isLike());
    }
}
