<?php

declare(strict_types=1);

namespace Glance\Search\Filter;

/**
 * Operator is the class capable of compiling a statement
 */
class Operator
{
    private const EQUAL = "=";

    private const EQUAL_OR_GREATER = ">=";

    private const EQUAL_OR_LESS = "<=";

    private const GREATER = ">";

    private const LESS = "<";

    private const CONTAIN = "contain";

    private const NOT_CONTAIN = "not-contain";

    private const NOT_EQUAL = "!=";

    private $compilerMethod;

    public function __construct(string $operatorName, string $customMatch = null)
    {
        $availableOperators = [
            self::EQUAL,
            self::EQUAL_OR_GREATER,
            self::EQUAL_OR_LESS,
            self::GREATER,
            self::LESS,
            self::CONTAIN,
            self::NOT_CONTAIN,
            self::NOT_EQUAL
        ];

        if (!in_array($operatorName, $availableOperators)) {
            throw new \InvalidArgumentException("Operator not supported");
        }

        $this->compilerMethod = $this->getCompilerMethod($operatorName, $customMatch);
    }

    private function getCompilerMethod($operatorName, $customMatch)
    {
        if ($customMatch) {
            return "compileUsingCustomMatch";
        }

        switch ($operatorName) {
            case self::EQUAL:
                return "compileEqual";
                break;
            case self::EQUAL_OR_GREATER:
                return "compileEqualOrGreater";
                break;
            case self::EQUAL_OR_LESS:
                return "compileEqualOrLess";
                break;
            case self::GREATER:
                return "compileGreater";
                break;
            case self::LESS:
                return "compileLess";
                break;
            case self::CONTAIN:
                return "compileContain";
                break;
            case self::NOT_CONTAIN:
                return "compileNotContain";
                break;
            case self::NOT_EQUAL:
                return "compileNotEqual";
                break;
            default:
                break;
        }
    }

    private function compileContain(?string $columnName, ?string $value)
    {
        if (!$value) {
            return "$columnName is NULL";
        }

        return "UPPER($columnName) LIKE UPPER('%$value%')";
    }

    private function compileNotContain(?string $columnName, ?string $value)
    {
        if (!$value) {
            return "$columnName is NOT NULL";
        }

        return "UPPER($columnName) NOT LIKE UPPER('%$value%')";
    }

    private function compileEqual(?string $columnName, ?string $value, ?string $type, ?string $format)
    {
        if (!$value) {
            return "$columnName is NULL";
        }

        if ($type == "date") {
            return "TO_CHAR($columnName, '$format') = '$value'";
        }

        return "UPPER($columnName) = UPPER('$value')";
    }

    private function compileNotEqual(?string $columnName, ?string $value, ?string $type, ?string $format)
    {
        if (!$value) {
            return "$columnName is NOT NULL";
        }

        if ($type == "date") {
            return "TO_CHAR($columnName, '$format') != '$value'";
        }

        return "UPPER($columnName) != UPPER('$value')";
    }

    private function compileGreater(?string $columnName, ?string $value, ?string $fieldType, ?string $format)
    {
        if ($fieldType == "date") {
            return "$columnName > TO_DATE('$value', '$format')";
        }

        return "$columnName > $value";
    }

    private function compileLess(?string $columnName, ?string $value, ?string $fieldType, ?string $format)
    {
        if ($fieldType == "date") {
            return "$columnName < TO_DATE('$value', '$format')";
        }

        return "$columnName < $value";
    }

    private function compileEqualOrGreater(?string $columnName, ?string $value, ?string $fieldType, ?string $format)
    {
        if ($fieldType == "date") {
            return "$columnName >= TO_DATE('$value', '$format')";
        }

        return "$columnName >= $value";
    }

    private function compileEqualOrLess(?string $columnName, ?string $value, ?string $fieldType, ?string $format)
    {
        if ($fieldType == "date") {
            return "$columnName <= TO_DATE('$value', '$format')";
        }

        return "$columnName <= $value";
    }

    private function compileUsingCustomMatch(
        ?string $columnName,
        ?string $value,
        ?string $fieldType,
        ?string $format,
        string $customMatch
    ) {
        return str_replace("__VALUE__", $value, $customMatch);
    }

    public function compile(?string $columnName, ?string $value, ?string $fieldType, ?string $format, ?string $customMatch)
    {
        return $this->{$this->compilerMethod}($columnName, $value, $fieldType, $format, $customMatch);
    }

    public static function isOperatorString(string $word)
    {
        $availableOperators = [
            self::EQUAL,
            self::EQUAL_OR_GREATER,
            self::EQUAL_OR_LESS,
            self::GREATER,
            self::LESS,
            self::CONTAIN,
            self::NOT_CONTAIN,
            self::NOT_EQUAL
        ];
        return in_array($word, $availableOperators);
    }
}
