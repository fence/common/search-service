<?php

declare(strict_types=1);

namespace Glance\Search\Filter;

/**
 * Statement is a string composed of a field + an operator + a value
 */
abstract class Statement
{
    private const EMPTY_VALUE = "__EMPTY__";

    public static function compiled(
        string $fieldName,
        string $operatorName,
        ?string $value,
        array $configuration
    ): ?string {

        $fieldName = self::decode($fieldName);
        $value = self::decode($value);

        $fieldConfiguration = (isset($configuration[$fieldName])) ? $configuration[$fieldName] : null;

        if (!$fieldConfiguration) {
            throw new \InvalidArgumentException("Missing field configuration on search configuration file for $fieldName");
        }

        if (
            isset($fieldConfiguration["compatibleOperators"])
            && !in_array($operatorName, $fieldConfiguration["compatibleOperators"])
        ) {
            throw new \InvalidArgumentException("Invalid operator for field $fieldName");
        }

        /**
         * Field type and format only really matter when comparing dates, so they can be set to
         * null in case no configuration is provided
         */
        $fieldType = isset($fieldConfiguration["type"]) ? $fieldConfiguration["type"] : null;
        $fieldFormat = isset($fieldConfiguration["format"]) ? $fieldConfiguration["format"] : null;

        /**
         * If a column mapper configuration is provided, the mapped value is going to be used
         */
        $columnName = isset($fieldConfiguration["column"]) ? $fieldConfiguration["column"] : $fieldName;

        $customMatch = self::getCustomMatch($fieldConfiguration, $operatorName);

        $operator = new Operator($operatorName, $customMatch);

        return $operator->compile($columnName, $value, $fieldType, $fieldFormat, $customMatch);
    }

    private static function decode(string $string): ?string
    {
        if ($string == self::EMPTY_VALUE) {
            return null;
        }

        return str_replace("__", " ", $string);
    }

    private static function getCustomMatch(
        array $fieldConfiguration,
        string $operator
    ) {
        if (!isset($fieldConfiguration["customMatch"])) {
            return null;
        }

        $customMatchConfiguration = $fieldConfiguration["customMatch"];

        foreach ($customMatchConfiguration as $customMatch) {
            if ($customMatch["operator"] == $operator) {
                return $customMatch["match"];
            }
        }
    }
}
