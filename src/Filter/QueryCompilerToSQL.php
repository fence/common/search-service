<?php

declare(strict_types=1);

namespace Glance\Search\Filter;

use InvalidArgumentException;

/**
 * Class that knows how to divide the input query string into smaller parts. These parts can be
 * Statements, grouping marks or conjunctions.
 *
 * Statements are composed by a Field + an Operator + a value
 * Grouping marks are ")" or "("
 * Conjunctions are "AND" or "OR"
 *
 * When making null comparisons, the __EMPTY__ string should be used. For example:
 *  (responsibleName = Gabriel OR responsibleName = __EMPTY__)
 *
 * Fields and Values should be surrounded by "
 */
class QueryCompilerToSQL
{
    /**
     * Compiles the query string to a SQL filter
     */
    public function compile(string $query, array $searchConfiguration)
    {
        /*
         * Eg.: $encodedQueryIndividualWordsAsArray = ["responsibleName", "contain", "Gabriel_Jose"];
        */
        $encodedQueryIndividualWordsAsArray = explode(" ", $this->encodeFieldAndValueSpaces($query));
        $compiledQuery = [];

        $counter = 0;
        foreach ($encodedQueryIndividualWordsAsArray as $queryComponent) {
            if (Operator::isOperatorString($queryComponent)) {
                if (!isset($encodedQueryIndividualWordsAsArray[$counter - 1])) {
                    throw new InvalidArgumentException(
                        "Invalid query: missing field"
                    );
                }
                if (!isset($encodedQueryIndividualWordsAsArray[$counter + 1])) {
                    throw new InvalidArgumentException(
                        "Invalid query: missing value"
                    );
                }
                $compiledQuery[] = Statement::compiled(
                    $encodedQueryIndividualWordsAsArray[$counter - 1],       // statement field
                    $encodedQueryIndividualWordsAsArray[$counter],           // statement operator
                    $encodedQueryIndividualWordsAsArray[$counter + 1],       // statement value
                    $searchConfiguration                                     // mapping file
                );
            }

            if ($this->isGroupingMark($queryComponent)) {
                $compiledQuery[] = $queryComponent;
            }

            if ($this->isConjunction($queryComponent)) {
                $compiledQuery[] = $queryComponent;
            }
            $counter++;
        }

        $compiledStatements = join(" ", $compiledQuery);
        return $compiledStatements ? ("WHERE ($compiledStatements)") : '';
    }

    private function isGroupingMark(string $queryComponent)
    {
        return in_array($queryComponent, [")", "("]);
    }

    private function isConjunction(string $queryComponent)
    {
        return in_array($queryComponent, ["AND", "OR"]);
    }

    /**
     * Encodes Field's and Value's spaces
     *
     * Eg.:
     * input:  "responsible institutes" contain "Universidade de Santiago de Compostela";
     * output: "responsible__institutes contain Universidade__de__Santiago__de__Compostela";
     */
    private function encodeFieldAndValueSpaces(string $rawQuery)
    {
        $rawQueryAsArray = str_split($rawQuery);

        for ($i = 0; $i < count($rawQueryAsArray); $i++) {
            if ($rawQueryAsArray[$i] == '"') {
                $i++; // skips the current "
                while ($rawQueryAsArray[$i] != '"') {
                    if ($rawQueryAsArray[$i] == " ") {
                        $rawQueryAsArray[$i] = "__";
                    }
                    $i++;
                }
            }
        }

        $encodedQuery = join("", $rawQueryAsArray);
        $encodedQuery = str_replace('"', " ", $encodedQuery);
        $encodedQuery = preg_replace("!\s+!", " ", $encodedQuery);

        return $encodedQuery;
    }
}
